#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Image writer


import io, sys, os, time, re, subprocess, logging
import fcntl
import shlex

logger = logging.getLogger(__name__)

def getsize(path, default=None):
	size = os.path.getsize(path)
	if path == "/dev/zero":
		return default
	if not os.path.isfile(path):
		size = int(subprocess.check_output(["blockdev", "--getsize64", path]).decode().rstrip())
	return size

class fdopen:
	# TODO context managerize
	def __init__(self, path, method="zstd"):
		self._path = path
		self._cmd = ["sh", "-c", "cat {} | {} -d".format(shlex.quote(path), method)]
		self._proc = subprocess.Popen(self._cmd, stdout=subprocess.PIPE)

	def read(self, l):
		data = self._proc.stdout.read(l)
		return data

	def seek(self, pos, whence):
		raise NotImplementedError()

	def close(self):
		self._proc.wait()

def open(src):
	src_size = 1234567890123456789
	if src.endswith((".zstd", ".zst")):
		fi = fdopen(src, method="zstd")
	elif src.endswith(".bz2"):
		fi = fdopen(src, method="bzip2")
	elif src.endswith(".xz"):
		fi = fdopen(src, method="xz")
	else:
		fi = io.open(src, "rb")
		src_size = fi.seek(0, os.SEEK_END)
		fi.seek(0)
	return fi, src_size

def make_disk(src, dst,
 block=(1024<<10),
 zero_after=False,#True,
 verify=True,
 progress_to_stdout=False,
 errors_warn=False,
 ):
	fi, src_size = open(src)

	logger.info("Making disk %s with %s (size %d)", dst, src, src_size)

	if os.path.exists(dst):
		fo = io.open(dst, "r+b")
	else:
		fo = io.open(dst, "w+b")

	# This is useless
	cur_fl = fcntl.fcntl(fo.fileno(), fcntl.F_GETFL, 0)
	l_flags = cur_fl | os.O_SYNC
	res = fcntl.fcntl(fo.fileno(), fcntl.F_SETFL, l_flags)

	need_verif = False

	t0 = time.time()

	writes = 0
	todo = src_size
	done = 0
	while todo != 0:
		cur = min(todo, block)
		if progress_to_stdout:
			sys.stdout.write("<")
			sys.stdout.flush()

		chunk = fi.read(cur)
		cur = len(chunk)

		if chunk == b"":
			break

		for idx_rb in range(10):

			fo.seek(done)
			if idx_rb == 0 and False:
				chunk_rb = None
			else:
				chunk_rb = fo.read(cur)

			if chunk == chunk_rb:
				if progress_to_stdout:
					sys.stdout.write("\x1B[32;1m!\x1B[0m")
				break
			elif idx_rb == 0:
				if progress_to_stdout:
					sys.stdout.write("W")
				need_verif = True
			else:
				if progress_to_stdout:
					sys.stdout.write("\x1B[31;1m!\x1B[0m")
					sys.stdout.flush()

			fo.seek(done)
			try:
				fo.write(chunk)
			except Exception as e:
				if errors_warn:
					sys.stdout.write("\x1B[33;1m!\x1B[0m")
				else:
					raise

			writes += len(chunk)

			try:
				fo.flush()
				os.fsync(fo.fileno())
			except Exception as e:
				if errors_warn:
					sys.stdout.write("\x1B[33;1m!\x1B[0m")
				else:
					raise

		todo -= cur
		done += cur

		if progress_to_stdout:
			sys.stdout.write(">")
			sys.stdout.flush()

	if progress_to_stdout:
		print("")

	t1 = time.time()
	logger.info("Wrote %d bytes in %.3f s, %.3f MB/s", writes, t1-t0, done / 1e6 / (t1-t0))

	if zero_after:
		dst_size = fo.seek(0, os.SEEK_END)

		chunk_template = b"\x00" * block
		todo = dst_size - src_size
		done = src_size
		while todo != 0:
			cur = min(todo, block)
			if progress_to_stdout:
				sys.stdout.write("{")
				sys.stdout.flush()

			chunk = chunk_template[:cur]

			if chunk == b"":
				break

			for idx_rb in range(10):

				fo.seek(done)
				chunk_rb = fo.read(cur)
				if chunk == chunk_rb:
					if progress_to_stdout:
						sys.stdout.write("\x1B[32;1m!\x1B[0m")
					break
				elif idx_rb == 0:
					if progress_to_stdout:
						sys.stdout.write("W")
					need_verif = True
				else:
					if progress_to_stdout:
						sys.stdout.write("\x1B[31;1m!\x1B[0m")
						sys.stdout.flush()

				fo.seek(done)
				fo.write(chunk)
				writes += len(chunk)

				fo.flush()
				os.fsync(fo.fileno())

			todo -= cur
			done += cur

			if progress_to_stdout:
				sys.stdout.write("}")
				sys.stdout.flush()

		if progress_to_stdout:
			print("")

		t2 = time.time()
		logger.info("Wrote %d bytes in %.3f s, %.3f MB/s", writes, t2-t1, done / 1e6 / (t2-t1))

	fo.close()
	fi.close()

	try:
		with open("/proc/sys/vm/drop_caches", "wb") as f:
			f.write(b"3\n")
	except:
		pass

	class BadMarker(object):
		def __init__(self):
			self._bads = list()
		def __call__(self, pos, size):
			logger.warning("Bad block at pos=%d size=%d", pos, size)
			self._bads.append((pos, size))
		def report(self):
			return self._bads

	class BadFixer(object):
		def __init__(self):
			self._bads = list()
			self._trials = 1
			self._fixed = list()
		def __call__(self, pos, size):
			logger.warning("Bad block at pos=%d size=%d", pos, size)
			self._bads.append((pos, size))
		def report(self):
			return self._bads

	if verify and need_verif:
		bad = BadMarker()
		#bad = BadFixer()

		logger.info("Checking disk %s with image %s", dst, src)

		fi, src_size = open(src)

		fo = io.open(dst, "rb")

		t0 = time.time()

		todo = src_size
		done = 0
		while todo != 0:
			cur = min(todo, block)
			if progress_to_stdout:
				sys.stdout.write("<")
				sys.stdout.flush()

			chunk_a = fi.read(cur)
			cur = len(chunk_a)

			if chunk_a == b"":
				todo = 0
				continue

			chunk_b = fo.read(cur)

			assert len(chunk_a) == cur
			assert len(chunk_b) == cur

			if chunk_a != chunk_b:
				bad(done, cur)

			todo -= cur
			done += cur
			if progress_to_stdout:
				sys.stdout.write(">")
				sys.stdout.flush()

		t1 = time.time()

		if progress_to_stdout:
			print("")

		logger.info("Verified %d bytes in %.3f s, %.3f MB/s",
		 done, t1-t0, done / 1e6 / (t1-t0))

		for pos, size in bad.report():
			logger.warning("- %d %d", pos, size)


	m = re.match("/dev/(?P<bdev>sd.*)", dst)
	if m is not None:
		bdev = m.group("bdev")
		with io.open("/sys/block/%s/device/delete" % bdev, "wb") as f:
			f.write("1\n".encode())
		logger.info("Ejected %s", dst)

def main():

	import argparse

	parser = argparse.ArgumentParser(
	 description="Image flasher",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("dst",
	 help="File/block device to write to",
	)

	parser.add_argument("src",
	 help="File/block device to read from",
	)

	default_mode = "dst_ge_src"
	parser.add_argument("--mode",
	 help="What to do",
	 default=default_mode,
	 choices=("dst_ge_src",),
	)

	parser.add_argument("--progress",
	 action="store_true",
	 default=False,
	)

	parser.add_argument("--sudo",
	 action="store_true",
	 default=False,
	)

	parser.add_argument("--block-size",
	 type=int,
	 default=(10<<20),
	)

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)


	if args.sudo:
		cmd = [
		 "sudo",
		 sys.executable,
		 __file__,
		] + [x for x in sys.argv[1:] if x != "--sudo" ]
		logger.info("Running with sudo: %s", cmd)
		return subprocess.call(cmd)

	make_disk(args.src, args.dst,
	 progress_to_stdout=args.progress,
	 block=args.block_size,
	)


if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
